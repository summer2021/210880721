<?php

/**
 * @package    Joomla.Administrator
 * @subpackage com_guidedtours
 *
 * @copyright (C) 2018 Open Source Matters, Inc. <https://www.joomla.org>
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

namespace Joomla\Component\Guidedtours\Administrator\Helper;

\defined('_JEXEC') or die;

use Joomla\CMS\Helper\ContentHelper;

/**
 * The first example class, this is in the same
 * package as declared at the start of file but
 * this example has a defined subpackage
 *
 * @since 4.0.0
 */
class StepHelper extends ContentHelper
{
}
